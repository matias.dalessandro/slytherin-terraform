#vpc_id = "aws_vpc.slytherin-tf.id"

cidr_block_vpc = "10.0.0.0/16"

cidr_block_subnets = ["10.0.1.0/24","10.0.2.0/24","10.0.3.0/24","10.0.4.0/24"]

cidr_block = "0.0.0.0/0"

eip = "true"

#id_allocation = ["aws_eip.eip-1.id","aws_eip.eip-2.id"]

#subnet_id = ["aws_subnet.public-subnet-1.id","aws_subnet.public-subnet-2.id","aws_subnet.private-subnet-1.id","aws_subnet.private-subnet-2.id"]

connectivity_type = "public"

#gateway_id = "aws_internet_gateway.igw.id"

#route_table_id = ["aws_route_table.rt-public.id","aws_route_table.rt-privada.id"]

#natgateway_id = "nat-gw-ps-1.id"