#----------------------------------------VPC-----------------------------------#

resource "aws_vpc" "slytherin-tf" {
  cidr_block       = var.cidr_block_vpc
  instance_tenancy = var.instance_tenancy

  tags = {
    Name = "slytherin-tf"
  }
}

#---------------------------------SUBNETS---------------------------------------#

resource "aws_subnet" "public-subnet-1" {
  vpc_id     = aws_vpc.slytherin-tf.id
  cidr_block = var.cidr_block_subnets[0]
  availability_zone = "us-east-1a"

  tags = {
    Name = "public-subnet-1"
  }
} 

resource "aws_subnet" "public-subnet-2" {
  vpc_id     = aws_vpc.slytherin-tf.id
  cidr_block = var.cidr_block_subnets[1]
  availability_zone = "us-east-1b"


  tags = {
    Name = "public-subnet-2"
  }
}

resource "aws_subnet" "private-subnet-1" {
  vpc_id     = aws_vpc.slytherin-tf.id
  cidr_block = var.cidr_block_subnets[2]
  availability_zone = "us-east-1a"

  tags = {
    Name = "private-subnet-1"
  }
}

resource "aws_subnet" "private-subnet-2" {
  vpc_id     = aws_vpc.slytherin-tf.id
  cidr_block = var.cidr_block_subnets[3]
  availability_zone = "us-east-1b"


  tags = {
    Name = "private-subnet-2"
  }
}

#--------------------------ELASTIC-IP---------------------#

resource "aws_eip" "eip-1" {
  vpc = var.eip

  tags = {
    Name = "eip-1"
  }
}

resource "aws_eip" "eip-2" {
  vpc = var.eip

  tags = {
    Name = "eip-2"
  }
}


#--------------------------INTERNET-GATEWAY---------------------#

resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.slytherin-tf.id
  tags = {
    Name = "igw"
  }
}


#------------------------NAT-GATEWAY---------------------------#

resource "aws_nat_gateway" "nat-gw-ps-1" {
  allocation_id = aws_eip.eip-1.id
  subnet_id     = aws_subnet.public-subnet-1.id
  connectivity_type = var.connectivity_type

  tags = {
    Name = "nat-gw-ps-1"
  }

  # To ensure proper ordering, it is recommended to add an explicit dependency
  # on the Internet Gateway for the VPC.
  #depends_on = [aws_internet_gateway.nat-gw-ps-1]
}

resource "aws_nat_gateway" "nat-gw-ps-2" {
  allocation_id =  aws_eip.eip-2.id
  subnet_id     = aws_subnet.public-subnet-2.id
  connectivity_type = var.connectivity_type

  tags = {
    Name = "nat-gw-ps-2"
  }

  # To ensure proper ordering, it is recommended to add an explicit dependency
  # on the Internet Gateway for the VPC.
  #depends_on = [aws_internet_gateway.nat-gw-ps-2]
}

#-------------------ROUTE-TABLE-----------------------#

resource "aws_route_table" "rt-public" {
  vpc_id = aws_vpc.slytherin-tf.id

  route {
    cidr_block = var.cidr_block
    gateway_id = aws_internet_gateway.igw.id
  }

  tags = {
    Name = "rt-public"
  }
}

resource "aws_route_table_association" "a" {
  subnet_id      = aws_subnet.public-subnet-1.id
  route_table_id = aws_route_table.rt-public.id
}
resource "aws_route_table_association" "b" {
  subnet_id      = aws_subnet.public-subnet-2.id
  route_table_id = aws_route_table.rt-public.id
}



resource "aws_route_table" "rt-privada" {
  vpc_id = aws_vpc.slytherin-tf.id

  route {
    cidr_block = var.cidr_block
    gateway_id = aws_nat_gateway.nat-gw-ps-1.id
  }

  tags = {
    Name = "rt-privada"
  }
}

resource "aws_route_table_association" "c" {
  subnet_id      = aws_subnet.private-subnet-1.id
  route_table_id = aws_route_table.rt-privada.id
}

resource "aws_route_table_association" "d" {
  subnet_id      = aws_subnet.private-subnet-2.id
  route_table_id = aws_route_table.rt-privada.id
}